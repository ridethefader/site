---
title: "Biography"
description: ""
draft: true
---
# Biography

Impassioned, melancholic and hopeful indierock of the kind that thrived in the 1990s. A triumvirate of guitar/vocals, drums and bass not unlike great trios of the 1990s such as Nirvana, Buffalo Tom, Dinosaur Jr and Sebadoh, which are all of great influence on the sound of Ride The Fader. But do not efface contemporary influences in the pursuit of a unique sound where melody and dynamics strive for a prominent place.  

Sounds like an amalgam of: Buffalo Tom, Death Cab For Cutie, Dinosaur Jr, Sebadoh, Lemonheads, Hüsker Dü, Bob Mould, Neil Young, Silversun Pickups.

![Ride The Fader](/images/mugshot_web.jpg "Ride The Fader")

Ride The Fader is, from left to right:  
Maarten Schouten - Bass  
Joris Molenaar - Drums  
Jeremy Jongepier - Guitar, vocals

## About the site

This site is built with [Hugo](https://gohugo.io/) using the [Minimal](https://github.com/calintat/minimal/) theme. It is hosted at [Hetzner](https://hetzner.com/). We care about the security and privacy of our visitors so we use as little external sources as possible and we log as little as possible. The only logging we do is used to keep out the bad folks and to be able to debug when things go wrong.

All content CC BY-NC 4.0. If you'd like to use our work on a commercial basis please drop us a note.
