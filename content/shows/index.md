---
title: "Shows"
date: 2022-08-22T20:19:03+02:00
draft: true
---
# Upcoming Shows

April 22 2023  
Record Store Day 2023  
BMM Games n' Vinyl, Beverwijk  
w/ The Tambles, Orange Skyline and Nova Sky

# Past Shows

## 2023

February 5 2023  
Gluren Bij De Buren, Beverwijk  
Black Snow City acoustic solo set  

## 2022

October 22 2022  
Muziekfort, Beverwijk  
w/ The Bintangs  

~~September 3 2022  
Fortenfestival, Muziekfort, Beverwijk  
w/ Gentle, Lorena + The Tide~~  

## 2018

December 19 2018  
Poterne, Muziekfort, Beverwijk  
Acoustic gig w/ Nova Sky  

## 2015

September 19 2015  
De Bakkerij, Castricum  
w/ Starmont  

August 1 2015  
Café Camille, Beverwijk  
Acoustic gig  

## 2014

April 26 2014  
Café Camille, Beverwijk  
Koningsdag 2014  

~~March 18 2014  
Café Lokaal, Heemskerk  
w/ Pale Angels~~  

January 4 2014  
De Bakkerij, Castricum  
w/ Buffalo Roam  

## 2013

November 9 2013  
De Bakkerij, Castricum  
North Empire Fest 1.5  
w/ Broadcaster (US), Muncie Girls (UK), All Aboard! (DE), Sweet Empire, The Windowsill, The Lost Noise  

November 2 2013  
SUB071, Leiden  
w/ The Cut/Up, Ken & Mary
