---
title: "Live at Fortenfestival"
date: 2022-08-25
tags: [ "fortenfestival", "muziekfort" ]
draft: true
---

Unfortunately the Fortenfestival has been cancelled. We've been offered to open for a great band on another date, more on that very soon!  

Saturday September 3 we will be playing at Fortenfestival, either in the Loods or if the weather is nice, on the outside stage of the Muziekfort. We will be sharing the stage with Gentle and [Lorena + The Tide](https://lorenaandthetide.com/), two other local bands. Fun fact, the drummer of Gentle is a son of the drummer of Jeremy's first band (Love is Killing). Great to see that the IJmond musical tradition is passed on to future generations!

We will play for about an hour starting from 21:00. Setlist looks like this so far:
- **Suffer And Bleed**  
  This one evolved from an Oasis-like kind of song into somewhat darker indie territory.
- **Break The Bond**  
  A red guitar, three chords and the truth.
- **Empty/Heavy Heart**  
  This is the first song where Dinosaur Sr. (aka "Neil Young") sneaks in. The bridge is heavily inspired by the song Mellow My Mind from what Jeremy thinks is one of the best, if not the best Neil Young album, Tonight's The Night
- **No Trace**  
  And there's Neil again, refered to in the lyrics. It's a song about being on the road. Neil Young and being on the road, great match.
- **Ill Conceived**  
  An almost The Who like 60's rocker
- **Leave It All Behind**  
  One of the band's favorites. A bit of a dark song, also because of the slight alternate tuning, and a hallmark example of what Ride The Fader is about. Slowly building up the tension to let it all out in the bridge. And that's not even at full gain, it's all about dynamics.
- **10 On The Richter Scale**  
  Jeremy was listening A LOT to Silversun Pickups when he came up with this one. That's not bad right? So it rocks and has this subcutaneous aspect to it.
- **We Got Time**  
  Our most recent creation, a bit like Buffalo Tom and Crazy Horse teaming up together.
- **Slow Down (Bear With Me)**
  And the oldest song on the list, also has this Silversun Pickups vibe with these big fuzzy choruses.

For more info and tickets check the [Muziekfort site](https://www.muziekfort.nl/fortenfestival.html).

![Fortenfestival](/images/flyer-fortenfestival.jpg "Fortenfestival")
