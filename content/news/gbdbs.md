---
title: "GBDB's"
date: 2023-02-12
draft: true
---

GBDB's, aka [Gluren Bij De Buren](https://glurenbijdeburen.nl/nl/programma/beverwijk#1102), was a great success! Jeremy played three acoustic sets of 30 minutes featuring Ride The Fader and Classic You! songs and an occasional cover, Diane from Hüsker Dü. For a small impression, check the [Black Snow City page]({{< ref "/blacksnowcity/index.md" >}}).

We welcomed about 40 people in our house throughout the whole day that were treated with coffee, freshly baked cookies and some great music. Many thanks to GBDB's for organizing the event and showing a different side of the cultural scene of Beverwijk. Next year BSC will definitely join again!
