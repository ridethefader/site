---
title: "Opening for The Bintangs"
date: 2022-09-09
draft: true
---

It must have been 1989. Had an eye on a red Hondo Strat clone at Molenaar Muziek in Beverwijk. My parents urged me to take someone with me to first check the guitar before buying it because I was going to buy it with my own savings and it would also be my first electric. That someone was Jan Wijte, the lead guitar player of The Bintangs in their heyday. Yes, it was actually Jan Wijte who got me into playing guitar. We've spent quite some holidays together with the Wijte family and there was always a guitar, one time even electric. Jan had built in an amp in the trunk of his car without his wife knowing it. The reaction when he opened up the trunk on the campsite in the south of France and plugged in his electric guitar, unforgettable. But the red Hondo, it got Jan's approval so I bought it and it has had quite a life. It ended up with Yuri Landman who made a new instrument out of it which was allegedly sold to Sonic Youth according to [internet lore](https://www.premierguitar.com/gear/diy-thurston-moores-drone-guitar-project).  

Last time I spoke with Jan was at the funeral of my mother, we played some songs together in honour of my mom. Unforgettable too. Even though he thought the amp he borrowed sounded like shit he pulled some amazing solo's. The Dutch Slow Hand. Definitely.  

So that's actually a close link to The Bintangs. Jan doesn't play that often with The Bintangs anymore but if it happens ever again, go see them! Also my parents in law own a cottage in the Bourgogne together with the Kraaijeveld family and some other families so there's another link. And we've lived at the Bergerslaan for some years and I wouldn't be surpised if The Bintangs had their rehearsal space in our storage back in the Sixties.  

Enough storytelling. The current setup of The Bintangs is really amazing too, especially Marco, without a doubt one of the best guitarists from around here. A Telecaster, a Blues Driver and a vintage Tweed amp is all he needs to nail that great Bintangs vibe. But ok, it was Marco who asked us to open for them and we gladly accepted the offer. So time to dust off the setlist and polish up the songs we're going to play. And Joris has to recover from a broken hand but we're all so stoked we can do this show that we know what's it gonna be. Allrigh, allright!

See you October 22nd in the Loods at the Muziekfort! Get your tickets [here](https://www.ticketkantoor.nl/shop/bintangs)!
