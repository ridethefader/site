---
title: "Black Snow City"
description: ""
draft: true
---

# Black Snow City

Black Snow City is het soloproject van Jeremy Jongepier, zanger/gitarist van Ride The Fader en zanger van Classic You! In de hoedanigheid van Black Snow City verzorgt hij akoestische interpretaties van nummers van eerdergenoemde bands aangevuld met eigen werk en wellicht een verdwaalde cover van één van zijn helden.

{{< video src="black_snow_city-empty_heavy_heart" width="400px" >}}
