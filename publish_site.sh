#!/bin/bash

hugo -D
read -p "Git commit message: " commit_message
git commit -am "$commit_message"
git push
ssh -i ~/.ssh/private/hetzner ridethefader_www@web.autostatic.net "git -C /srv/www/ridethefader.com/ pull"
